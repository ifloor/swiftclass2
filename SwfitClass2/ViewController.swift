//
//  ViewController.swift
//  SwfitClass2
//
//  Created by Igor Maldonado Floor on 13/02/18.
//  Copyright © 2018 IgorMaldonado. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var textView: UITextView!
    
    @IBAction func helloButtonTapped(_ sender: Any) {
        print("Hello Button tapped")
    }
    
    @IBAction func txtButtonTapped(_ sender: Any) {
        let textTyped: String = self.textField.text ?? ""
        self.textField.text = ""
        
        let textInView: String  = self.textView.text
        self.textView.text = "\(textInView)\n\(textTyped)"
    }
    
    @IBAction func detailButtonTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "detailViewController", sender: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.textView.isEditable = false
        self.textView.text = ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func demoButtonTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "GoToDemoHome", sender: nil)
    }
    
    @IBAction func assetButtonTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "assetsViewController", sender: nil)
    }
    
    
}

