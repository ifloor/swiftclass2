//
//  ProgressViewController.swift
//  SwfitClass2
//
//  Created by Igor Maldonado Floor on 15/02/18.
//  Copyright © 2018 IgorMaldonado. All rights reserved.
//

import UIKit
import WebKit

class ProgressViewController: UIViewController {

    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var webView: WKWebView!
    
    
    var loadingProgress = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.webView.load(URLRequest(url: URL(string: "http://youtube.com")!))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func progressButtonTapped(_ sender: Any) {
        if ( !loadingProgress ) {
            loadingProgress = true
            self.progressView.progress = 0
            
            self.progressTicked()
            
            
        }
    }
    
    func progressTicked() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            let currentProgress = self.progressView.progress
            
            if ( currentProgress < 1 ) {
                self.progressView.progress = currentProgress + 0.05
                self.progressTicked()
            } else {
                self.loadingProgress = false
            }
        }
    }
    
}
