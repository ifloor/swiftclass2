//
//  DemoHomeViewController.swift
//  SwfitClass2
//
//  Created by Igor Maldonado Floor on 14/02/18.
//  Copyright © 2018 IgorMaldonado. All rights reserved.
//

import UIKit

class DemoHomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func loaderButtonTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "GoToLoader", sender: nil)
    }
    
    @IBAction func progressButtonTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "GoToProgress", sender: nil)
        
    }
    
    
    @IBAction func datePickerButtonTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "GoDatePicker", sender: nil)
    }
}
