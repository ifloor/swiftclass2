//
//  DatePickerViewController.swift
//  SwfitClass2
//
//  Created by Igor Maldonado Floor on 15/02/18.
//  Copyright © 2018 IgorMaldonado. All rights reserved.
//

import UIKit

class DatePickerViewController: UIViewController {

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var sliderValueLabel: UILabel!
    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var stepperValueLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.pageControl.currentPage = 0
        self.pageControl.numberOfPages = 5
        
        self.sliderValueLabel.text = "\(self.slider.value)"
        
        self.stepperValueLabel.text = "\(self.stepper.value)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func slideChanged(_ sender: Any) {
        self.sliderValueLabel.text = "\(self.slider.value)"
    }
    
    @IBAction func stepperValueChanged(_ sender: Any) {
        self.stepperValueLabel.text = "\(self.stepper.value)"
    }
    
}
